let securityHeaders = {
    "Content-Security-Policy-Report-Only" : "default-src 'none'; form-action 'none'; frame-ancestors 'none'; report-uri https://lucassymons.report-uri.com/r/d/csp/wizard",
	"Strict-Transport-Security" : "max-age=1000",
	"X-Xss-Protection" : "1; mode=block",
	"X-Frame-Options" : "DENY",
	"X-Content-Type-Options" : "nosniff",
	"Referrer-Policy" : "strict-origin-when-cross-origin",
	"Feature-Policy" : "camera 'none'; geolocation 'none'; microphone 'none'",
    "Report-To" : "{'group':'default','max_age':31536000,'endpoints':[{'url':'https://lucassymons.report-uri.com/a/d/g'}],'include_subdomains':true}'",
    "NEL": "{'report_to':'default','max_age':31536000,'include_subdomains':true}",
}

let sanitiseHeaders = {
	"Server" : "Cloudflare",
}

let removeHeaders = [
	"Public-Key-Pins",
	"X-Powered-By",
	"X-AspNet-Version",
]

addEventListener('fetch', event => {
	event.respondWith(addHeaders(event.request))
})

async function addHeaders(req) {
	let response = await fetch(req)
	let newHdrs = new Headers(response.headers)

	if (newHdrs.has("Content-Type") && !newHdrs.get("Content-Type").includes("text/html")) {
		return new Response(response.body , {
			status: response.status,
			statusText: response.statusText,
			headers: newHdrs
		})
	}

	let setHeaders = Object.assign({}, securityHeaders, sanitiseHeaders)

	Object.keys(setHeaders).forEach(name => {
		newHdrs.set(name, setHeaders[name]);
	})

	removeHeaders.forEach(name => {
		newHdrs.delete(name)
	})

	return new Response(response.body , {
		status: response.status,
		statusText: response.statusText,
		headers: newHdrs
	})
}